# Advent of code 2016 #
http://adventofcode.com/2016


## Day 1 ##
|            |Part 1         | Part 2 |
|------------|---------------|--------|
|Python 2.7.3| 0.9 msec      | 20 msec|
|C++ 11      | 0.3 msec      | 41 msec|
|C++ 11 V2     | 60 μsec      | 1.4 msec|
|C++ 11 V2 -O2 | 50 μsec    | 145 μsec |

## Day 2 ##
|            |Part 1         | Part 2 |
|------------|---------------|--------|
|Python 2.7.3| - msec        | - msec |
|C++ 11      | 0.2 msec      | 8 msec |

## Day 3 ##
|            |Part 1         | Part 2 |
|------------|---------------|--------|
|Python 2.7.3| 4 msec        | 7 msec |
|C++ 11      | 11 msec       | 8 msec |
|C++ 11 V2 -O2 | 1.8 msec    | 8 msec |

## Day 4 ##
|            |Part 1         | Part 2 |
|------------|---------------|--------|
|Python 2.7.3| 15 msec       | 26 msec|
|pypy 2.2.1  | 11 msec       | 11 msec|
|C++ 11      | - msec        | - msec |

## Day 5 ##
|            |Part 1         | Part 2 |
|------------|---------------|--------|
|Python 2.7.3| - msec        | - msec |
|C++ 11      | 11.8 sec      |10.8 sec|

## Day 6 ##
|            |Part 1         | Part 2 |
|------------|---------------|--------|
|Python 2.7.3| 2.3 msec      |2.3 msec|
|C++ 11      | 3.1 msec      |4.5 msec|

## Day 7 ##
|            |Part 1         | Part 2 |
|------------|---------------|--------|
|Python 2.7.3| 58 msec       |38 msec |
|C++ 11      | 3.1 msec      |4.5 msec|

## Day 13 ##
|            |Part 1         | Part 2 |
|------------|---------------|--------|
|Python 2.7.3| 58 msec       |38 msec |
|C++ 11      | 12 msec       | 6 msec |
